﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebTaskin.Startup))]
namespace WebTaskin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
