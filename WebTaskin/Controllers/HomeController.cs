﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebTaskin.Data;

namespace WebTaskin.Controllers
{
    public class HomeController : Controller
    {
        DBContext db = new DBContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Hakkimizda()
        {
            return View();
        }

        public ActionResult Rastgele()
        {
            Random rastgele = new Random();
            int id = rastgele.Next(1,2);

            var model = db.Makales.Where(x => x.MakaleId == id).ToList();

            return View(model);
        }

        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang,string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}